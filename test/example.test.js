// test/example.test.js

const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {
let random = Math.random;
let myvar = undefined;

before(() => {
    myvar = 1; // setup before testing
    console.log('Before testing... ');
})
    
    it('Myvar should exists', () => {
        should.exist(myvar);
    })

    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1); // 1 + 1
        expect(result).to.equal(2); // result expected to be equal 2
    });
    it('Should return 3 when using minus function with a=4, b=1', () => {
        const result = mylib.minus(4,1); // 4 - 1
        expect(result).to.equal(3); // result expected to be equal 2
    });

    it('Random', () => {
        const result = mylib.sum(random, random);
        expect(result).to.equal(random+random);
    })
    it('Is bigger than', () => {
        assert(100 >= 90) // true
    });
    after(() => {
        console.log('After testing... ');
    })
})