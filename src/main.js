// scr/main.js

const express = require('express');
const { arrayGen } = require('./mylib');
const app = express();
const port = 3000;
const mylib = require('./mylib');

console.log({ 
    sum: mylib.sum(1,1), 
    minus: mylib.minus(4,1),
    random: mylib.random(),
    arrayGen: mylib.arrayGen()
})

 app.get('/add', (req,res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(`${mylib.sum(a,b)}`);
}) 

app.get('/', (req, res) => {
    res.send('Hello World! Unit Testing.');
});

app.listen(port, () => {
    console.log(`Server: http://localhost:${port}`);
})